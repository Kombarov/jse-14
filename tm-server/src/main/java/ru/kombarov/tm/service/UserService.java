package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.user.IUserService;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.User;
import ru.kombarov.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

public final class UserService implements IUserService {

    @NotNull
    private final Bootstrap bootstrap;

    public UserService(final @NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @Override
    public void persist(final @Nullable User user) throws Exception {
        if (user == null) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull UserRepository userRepository = new UserRepository(em);
        userRepository.persist(user);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void merge(final @Nullable User user) throws Exception {
        if (user == null) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        UserRepository userRepository = new UserRepository(em);
        userRepository.merge(user);
        em.getTransaction().commit();
        em.close();
    }

    @NotNull
    @Override
    public List<User> findAll() {
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<User> users = new UserRepository(em).findAll();
        em.getTransaction().commit();
        em.close();
        return users;
    }

    @Nullable
    @Override
    public User findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @Nullable User user = new UserRepository(em).findOne(id);
        em.getTransaction().commit();
        em.close();
        return user;
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull UserRepository userRepository = new UserRepository(em);
        userRepository.remove(id);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void removeAll() {
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        UserRepository userRepository = new UserRepository(em);
        userRepository.removeAll();
        em.getTransaction().commit();
        em.close();
    }

    @Nullable
    @Override
    public User getUserByLogin(final @Nullable String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @Nullable User user;
        try {
            user = new UserRepository(em).getUserByLogin(login);
        }
        catch (NoResultException e) {
            return null;
        }
        em.getTransaction().commit();
        em.close();
        return user;
    }
}