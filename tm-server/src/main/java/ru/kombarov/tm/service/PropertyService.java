package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;

import java.io.FileInputStream;
import java.util.Properties;

import static ru.kombarov.tm.constant.DataConstant.FILE_PROPERTIES;

public final class PropertyService {

    @NotNull
    private final Properties property = new Properties();

    @NotNull
    public String getLogin() throws Exception {
        @NotNull
        final FileInputStream fileInputStream = new FileInputStream(FILE_PROPERTIES);
        property.load(fileInputStream);
        return property.getProperty("login");
    }

    @NotNull
    public String getUrl() throws Exception {
        @NotNull
        final FileInputStream fileInputStream = new FileInputStream(FILE_PROPERTIES);
        property.load(fileInputStream);
        return property.getProperty("url");
    }

    @NotNull
    public String getPassword() throws Exception {
        @NotNull
        final FileInputStream fileInputStream = new FileInputStream(FILE_PROPERTIES);
        property.load(fileInputStream);
        return property.getProperty("password");
    }

    @NotNull
    public String getDriver() throws Exception {
        @NotNull
        final FileInputStream fileInputStream = new FileInputStream(FILE_PROPERTIES);
        property.load(fileInputStream);
        return property.getProperty("driver");
    }
}
