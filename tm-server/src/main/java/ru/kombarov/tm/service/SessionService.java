package ru.kombarov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.session.ISessionService;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.repository.SessionRepository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    private final Bootstrap bootstrap;

    public SessionService(final @NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull List<Session> sessions = new SessionRepository(em).findAll();
        em.getTransaction().commit();
        em.close();
        return sessions;
    }

    @Nullable
    @Override
    public Session findOne(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @Nullable Session session;
        try {
            session = new SessionRepository(em).findOne(id);
        }
        catch (NoResultException e) {
            return null;
        }
        em.getTransaction().commit();
        em.close();
        return session;
    }

    @Override
    public void persist(final @Nullable Session session) throws Exception {
        if (session == null) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull SessionRepository sessionRepository = new SessionRepository(em);
        sessionRepository.persist(session);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void merge(final @Nullable Session session) throws Exception {
        if (session == null) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull SessionRepository sessionRepository = new SessionRepository(em);
        sessionRepository.merge(session);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void remove(final @Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull SessionRepository sessionRepository = new SessionRepository(em);
        sessionRepository.remove(id);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void removeAll() {
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull SessionRepository sessionRepository = new SessionRepository(em);
        sessionRepository.removeAll();
        em.getTransaction().commit();
        em.close();
    }

    @Nullable
    @Override
    public Session findOne(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @Nullable Session session;
        try {
            session = new SessionRepository(em).findOneByUserId(userId, id);
        }
        catch (NoResultException e) {
            return null;
        }
        em.getTransaction().commit();
        em.close();
        return session;
    }

    @Override
    public void remove(final @Nullable String userId, final @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        final @NotNull EntityManager em = bootstrap.getEntityManager();
        em.getTransaction().begin();
        final @NotNull SessionRepository sessionRepository = new SessionRepository(em);
        sessionRepository.removeByUserId(userId, id);
        em.getTransaction().commit();
        em.close();
    }
}
