package ru.kombarov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.datatransfer.SessionDTO;
import ru.kombarov.tm.entity.Session;
import ru.kombarov.tm.service.SessionService;

import java.util.Date;

import static ru.kombarov.tm.constant.AppConstant.*;
import static ru.kombarov.tm.util.SignatureUtil.sign;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @NotNull
    Bootstrap bootstrap;

    public AbstractEndpoint(final @NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void validateSession(@Nullable final SessionDTO userSession) throws Exception {
        if(userSession == null) throw new Exception("Method is unavailable for unauthorized users");
        final @Nullable Session session = bootstrap.getSessionService().findOne(userSession.getUserId(), userSession.getId());
        userSession.setSignature(null);
        if(session == null) throw new Exception("Session does not exist");
        if(!session.getSignature().equals(sign(userSession, SALT, CICLE))) throw new Exception("Session is invalid");
        if(new Date().getTime() - userSession.getTimestamp() > SESSION_LIFE_TIME) {
            bootstrap.getSessionService().remove(userSession.getUserId(), userSession.getId());
            throw new Exception("Session time is out, you need to log in again");
        }
    }
}
