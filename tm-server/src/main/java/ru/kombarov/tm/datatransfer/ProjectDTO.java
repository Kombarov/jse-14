package ru.kombarov.tm.datatransfer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.context.Bootstrap;
import ru.kombarov.tm.entity.Project;
import ru.kombarov.tm.enumerated.Status;

import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
public final class ProjectDTO extends AbstractEntityDTO {

    @Nullable
    private String name;

    @NotNull
    private String userId;

    @Nullable
    private String description;

    @Nullable
    private Date dateStart;

    @Nullable
    private Date dateFinish;

    @NotNull
    private Status status = Status.PLANNED;

    @NotNull
    public static Project toProject(final @NotNull Bootstrap bootstrap, final @NotNull ProjectDTO projectDTO) {
        final @NotNull Project project = new Project();
        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setUserId(projectDTO.getUserId());
        project.setDescription(projectDTO.getDescription());
        project.setDateStart(projectDTO.getDateStart());
        project.setDateFinish(projectDTO.getDateFinish());
        project.setStatus(projectDTO.getStatus());
        return project;
    }
}
