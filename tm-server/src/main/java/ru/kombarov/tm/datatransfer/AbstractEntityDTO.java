package ru.kombarov.tm.datatransfer;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public class AbstractEntityDTO implements Serializable {

    private static final long SerialVersionUID = 1L;

    @NotNull
    private String id = UUID.randomUUID().toString();
}
