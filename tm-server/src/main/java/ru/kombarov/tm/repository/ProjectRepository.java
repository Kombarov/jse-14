package ru.kombarov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.project.IProjectRepository;
import ru.kombarov.tm.entity.Project;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class ProjectRepository extends AbstractRepository implements IProjectRepository {

    public ProjectRepository(final @NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void persist(final @NotNull Project project) {
        em.persist(project);
    }

    @Override
    public void merge(final @NotNull Project project) {
        em.merge(project);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return em.createQuery("SELECT p FROM Project p", Project.class).getResultList();
    }

    @Nullable
    @Override
    public Project findOne(final @NotNull String id) {
        return em.find(Project.class, id);
    }

    @Override
    public void remove(final @NotNull String id) {
        em.remove(findOne(id));
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM Project").executeUpdate();
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(final @NotNull String userId) {
        return em.createQuery("SELECT p FROM Project p WHERE p.userId= :userId",
                               Project.class).setParameter("userId", userId).getResultList();
    }

    @Nullable
    @Override
    public Project findOneByUserId(final @NotNull String userId, final @NotNull String id) {
        return em.createQuery("SELECT p FROM Project p WHERE p.id = :id and p.userId = :userId",
                               Project.class).setParameter("id", id).setParameter("userId", userId).getSingleResult();
    }

    @Override
    public void removeAllByUserId(final @NotNull String userId) {
        for(final @NotNull Project project : findAll()) {
            if(userId.equals(project.getUserId())) em.remove(project);
        }
    }

    @NotNull
    @Override
    public Project findByName(final @NotNull String userId, final @NotNull String name) {
        return em.createQuery("SELECT p FROM Project p WHERE p.userId= :userId AND p.name = :name",
                               Project.class).setParameter("userId", userId).setParameter("name", name).getSingleResult();
    }

    @NotNull
    @Override
    public List<Project> sortByDateStart(final @NotNull String userId) {
        return em.createQuery("SELECT p FROM Project p WHERE p.userId= :userId ORDER BY p.dateStart",
                               Project.class).setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<Project> sortByDateFinish(final @NotNull String userId) {
        return em.createQuery("SELECT p FROM Project p WHERE p.userId= :userId ORDER BY p.dateFinish",
                               Project.class).setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<Project> sortByStatus(final @NotNull String userId) {
        return em.createQuery("SELECT p FROM Project p WHERE p.userId= :userId ORDER BY FIELD(p.status, 'PLANNED', 'IN_PROCESS', 'DONE')",
                               Project.class).setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<Project> findByPart(final @NotNull String description, final @NotNull String userId) {
        return em.createQuery("SELECT p FROM Project p WHERE p.userId= :userId AND p.description = :description",
                               Project.class).setParameter("userId", userId).setParameter("description", description).getResultList();
    }
}
