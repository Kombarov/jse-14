package ru.kombarov.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.session.ISessionRepository;
import ru.kombarov.tm.entity.Session;

import javax.persistence.EntityManager;
import java.util.List;

@NoArgsConstructor
public final class SessionRepository extends AbstractRepository implements ISessionRepository {

    public SessionRepository(final @NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return em.createQuery("SELECT s FROM Session s", Session.class).getResultList();
    }

    @Nullable
    @Override
    public Session findOne(final @NotNull String id) {
        return em.find(Session.class, id);
    }

    @Override
    public void persist(final @NotNull Session session) {
        em.persist(session);
    }

    @Override
    public void merge(final @NotNull Session session) {
        em.merge(session);
    }

    @Override
    public void remove(final @NotNull String id) {
        em.remove(findOne(id));
    }

    @Override
    public void removeAll() {
        em.createQuery("DELETE FROM Session").executeUpdate();
    }

    @Nullable
    @Override
    public Session findOneByUserId(final @NotNull String userId, final @NotNull String id) {
        return em.createQuery("SELECT s FROM Session s WHERE s.userId= :userId AND s.id= :id",
                               Session.class).setParameter("userId", userId).setParameter("id", id).getSingleResult();
    }

    @Override
    public void removeByUserId(final @NotNull String userId, final @NotNull String id) {
        em.createQuery("DELETE FROM Session s WHERE s.userId = :userId AND s.id = :id").setParameter("userId",
                        userId).setParameter("id", id).executeUpdate();
    }
}
