package ru.kombarov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.datatransfer.SessionDTO;
import ru.kombarov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "app_session")
public class Session extends AbstractEntity {

    @NotNull
    @Basic(optional = false)
    String userId;

    @NotNull
    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    Role role;

    @NotNull
    @Basic(optional = false)
    Long timestamp = new Date().getTime();

    @Basic
    @Nullable
    String signature;

    @NotNull
    public static SessionDTO toSessionDTO(final @NotNull Session session) {
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setUserId(session.getUserId());
        sessionDTO.setRole(session.getRole());
        sessionDTO.setTimestamp(session.getTimestamp());
        sessionDTO.setSignature(session.getSignature());
        return sessionDTO;
    }
}
