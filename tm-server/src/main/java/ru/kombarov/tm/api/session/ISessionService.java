package ru.kombarov.tm.api.session;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Session;

import java.util.List;

public interface ISessionService {

    @NotNull
    List<Session> findAll();

    @Nullable
    Session findOne(final @Nullable String id) throws Exception;

    void persist(final @Nullable Session session) throws Exception;

    void merge(final @Nullable Session session) throws Exception;

    void remove(final @Nullable String id) throws Exception;

    void removeAll();

    @Nullable
    Session findOne(final @Nullable String userId, final @Nullable String id) throws Exception;

    void remove(final @Nullable String userId, final @Nullable String id) throws Exception;
}
