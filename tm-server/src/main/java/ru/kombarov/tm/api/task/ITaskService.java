package ru.kombarov.tm.api.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Task;

import java.util.List;

public interface ITaskService {

    void persist(final @Nullable Task task) throws Exception;

    void merge(final @Nullable Task task) throws Exception;

    @NotNull
    List<Task> findAll() throws Exception;

    @Nullable
    Task findOne(final @Nullable String id) throws Exception;

    void remove(final @Nullable String id) throws Exception;

    void removeAll();

    @NotNull
    List<Task> findAll(final @Nullable String userId) throws Exception;

    @Nullable
    Task findOne(final @Nullable String userId, final @Nullable String id) throws Exception;

    void removeAll(final @Nullable String userId) throws Exception;

    @NotNull
    Task findByName(final @Nullable String userId, final @Nullable String name) throws Exception;

    @NotNull
    List<Task> getTasksByProjectId(final @Nullable String userId, final @Nullable String projectId) throws Exception;

    @NotNull
    List<Task> sortByDateStart(final @Nullable String userId) throws Exception;

    @NotNull
    List<Task> sortByDateFinish(final @Nullable String userId) throws Exception;

    @NotNull
    List<Task> sortByStatus(final @Nullable String userId) throws Exception;

    @NotNull
    List<Task> findByPart(final @Nullable String description, final @Nullable String userId) throws Exception;
}
