package ru.kombarov.tm.api.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Project;

import java.util.List;

public interface IProjectService {

    void persist(final @Nullable Project project) throws Exception;

    void merge(final @Nullable Project project) throws Exception;

    @NotNull
    List<Project> findAll();

    @Nullable
    Project findOne(final @Nullable String id) throws Exception;

    void remove(final @Nullable String id) throws Exception;

    void removeAll();

    @NotNull
    List<Project> findAll(final @Nullable String userId) throws Exception;

    @Nullable
    Project findOne(final @Nullable String userId, final @Nullable String id) throws Exception;

    void removeAll(final @Nullable String userId) throws Exception;

    @NotNull
    Project findByName(final @Nullable String userId, final @Nullable String name) throws Exception;

    @NotNull
    List<Project> sortByDateStart(final @Nullable String userId) throws Exception;

    @NotNull
    List<Project> sortByDateFinish(final @Nullable String userId) throws Exception;

    @NotNull
    List<Project> sortByStatus(final @Nullable String userId) throws Exception;

    @NotNull
    List<Project> findByPart(final @Nullable String description, final @Nullable String userId) throws Exception;
}
