package ru.kombarov.tm.api.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Project;

import java.util.List;

public interface IProjectRepository {

    void persist(final @NotNull Project project);

    void merge(final @NotNull Project project);

    @NotNull
    List<Project> findAll();

    @Nullable
    Project findOne(final @NotNull String id);

    void remove(final @NotNull String id);

    void removeAll();

    @NotNull
    List<Project> findAllByUserId(final @NotNull String userId);

    @Nullable
    Project findOneByUserId(final @NotNull String userId, final @NotNull String id);

    void removeAllByUserId(final @NotNull String userId);

    @NotNull
    Project findByName(final @NotNull String userId, final @NotNull String name);

    @NotNull
    List<Project> sortByDateStart(final @NotNull String userId);

    @NotNull
     List<Project> sortByDateFinish(final @NotNull String userId);

    @NotNull
    List<Project> sortByStatus(final @NotNull String userId);

    @NotNull
    List<Project> findByPart(final @NotNull String description, final @NotNull String userId);
}
