package ru.kombarov.tm.api.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.entity.Task;

import java.util.List;

public interface ITaskRepository {

    void persist(final @NotNull Task task);

    void merge(final @NotNull Task task);

    @NotNull
    List<Task> findAll();

    @Nullable
    Task findOne(final @NotNull String id);

    void remove(final @NotNull String id);

    void removeAll();

    @NotNull
    List<Task> findAllByUserId(final @NotNull String userId);

    @Nullable
    Task findOneByUserId(final @NotNull String userId, final @NotNull String id);

    void removeAllByUserId(final @NotNull String userId);

    @NotNull
    Task findByName(final @NotNull String userId, final @NotNull String name);

    @NotNull
    List<Task> getTasksByProjectId(final @NotNull String userId, final @NotNull String projectId);

    @NotNull
    List<Task> sortByDateStart(final @NotNull String userId);

    @NotNull
    List<Task> sortByDateFinish(final @NotNull String userId);

    @NotNull
    List<Task> sortByStatus(final @NotNull String userId);

    @NotNull
    List<Task> findByPart(final @NotNull String description, final @NotNull String userId);
}
