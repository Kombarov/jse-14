package ru.kombarov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.datatransfer.SessionDTO;
import ru.kombarov.tm.datatransfer.TaskDTO;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    void persistTask(final @Nullable SessionDTO sessionDTO, final @Nullable TaskDTO taskDTO) throws Exception;

    @WebMethod
    void mergeTask(final @Nullable SessionDTO sessionDTO, final @Nullable TaskDTO taskDTO) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTasks(final @Nullable SessionDTO sessionDTO) throws Exception;

    @Nullable
    @WebMethod
    TaskDTO findOneTask(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception;

    @WebMethod
    void removeTask(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception;

    @WebMethod
    void removeAllTasks(final @Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    @WebMethod
    TaskDTO findTasksByName(final @Nullable SessionDTO sessionDTO, final @Nullable String name) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> getTaskListByProjectId(final @Nullable SessionDTO sessionDTO, final @Nullable String projectId) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> findAllTasksByUserId(final @Nullable SessionDTO sessionDTO) throws Exception;

    @Nullable
    @WebMethod
    TaskDTO findOneTaskByUserId(final @Nullable SessionDTO sessionDTO, final @Nullable String id) throws Exception;

    @WebMethod
    void removeAllTasksByUserId(final @Nullable SessionDTO sessionDTO) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> sortTasksByDateStart(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> sortTasksByDateFinish(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> sortTasksByStatus(final @Nullable SessionDTO sessionDTO, final @Nullable String userId) throws Exception;

    @NotNull
    @WebMethod
    List<TaskDTO> findTasksByPart(final @Nullable SessionDTO sessionDTO, final @Nullable String description) throws Exception;
}
