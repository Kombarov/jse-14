package ru.kombarov.tm.util;

import org.jetbrains.annotations.NotNull;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public final class DateUtil {

    @NotNull
    private final static SimpleDateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");

    public static XMLGregorianCalendar parseStringToDate(final @NotNull String stringDate) throws Exception {
        @NotNull
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(dateFormatter.parse(stringDate, new ParsePosition(0)));
        @NotNull
        final XMLGregorianCalendar xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
        return xmlDate;
    }

    @NotNull
    public static String parseDateToString(final @NotNull XMLGregorianCalendar date) {
        @NotNull
        final Calendar calendar = date.toGregorianCalendar();
        @NotNull
        final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        return formatter.format(calendar.getTime());
    }
}
