package ru.kombarov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.ProjectDTO;
import ru.kombarov.tm.api.endpoint.TaskDTO;
import ru.kombarov.tm.api.endpoint.UserDTO;

import java.util.List;

import static ru.kombarov.tm.util.DateUtil.parseDateToString;

public final class EntityUtil {

    public static void printTask(final @Nullable TaskDTO task) throws Exception {
        if (task == null) throw new Exception();
        System.out.println("task name: " + task.getName());
        System.out.println("task description: " + task.getDescription());
        System.out.println("start date: " + parseDateToString(task.getDateStart()));
        System.out.println("end date: " + parseDateToString(task.getDateFinish()));
    }

    public static void printTasks(final @NotNull List<TaskDTO> tasks) {
        for (int i = 0; i < tasks.size(); i++) {
            System.out.println(i+1 + ". " + tasks.get(i).getName());
        }
    }

    public static void printProject(final @Nullable ProjectDTO project) throws Exception {
        if (project == null) throw new Exception();
        System.out.println("project name: " + project.getName());
        System.out.println("project description: " + project.getDescription());
        System.out.println("start date: " + parseDateToString(project.getDateStart()));
        System.out.println("end date: " + parseDateToString(project.getDateFinish()));
    }

    public static void printProjects(final @NotNull List<ProjectDTO> projects) {
        for (int i = 0; i < projects.size(); i++) {
            System.out.println(i+1 + ". " + projects.get(i).getName());
        }
    }

    public static void printUser(final @Nullable UserDTO user) throws Exception {
        if (user == null || user.getRole() == null) throw new Exception();
        System.out.println("username: " + user.getLogin());
        System.out.println("role: " + user.getRole().value() + "\n");
        System.out.println("projects:");
    }

    public static void printUsers(final @NotNull List<UserDTO> users) {
        for (int i = 0; i < users.size(); i++) {
            System.out.println(i+1 + ". " + users.get(i).getLogin());
        }
    }
}
