package ru.kombarov.tm.context;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.kombarov.tm.api.ServiceLocator;
import ru.kombarov.tm.api.endpoint.*;
import ru.kombarov.tm.command.AbstractCommand;
import ru.kombarov.tm.endpoint.ProjectEndpointService;
import ru.kombarov.tm.endpoint.SessionEndpointService;
import ru.kombarov.tm.endpoint.TaskEndpointService;
import ru.kombarov.tm.endpoint.UserEndpointService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.Exception;
import java.util.*;

public final class Bootstrap implements ServiceLocator {

    @Nullable private SessionDTO sessionDTO;

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpointService().getTaskEndpointPort();

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private final BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @Nullable
    @Override
    public SessionDTO getSessionDTO() {
        return sessionDTO;
    }

    @Override
    public void setSessionDTO(final @Nullable SessionDTO sessionDTO) {
        this.sessionDTO = sessionDTO;
    }

    @NotNull
    public List<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

    @NotNull
    @Override
    public IProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public ITaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

    @NotNull
    @Override
    public ISessionEndpoint getSessionEndpoint() {
        return sessionEndpoint;
    }

    public void init() throws Exception {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        final @NotNull Reflections reflections = new Reflections("ru.kombarov.tm.command");
        final @NotNull Set<Class<? extends AbstractCommand>> classes = reflections.getSubTypesOf(ru.kombarov.tm.command.AbstractCommand.class);
        for (final @NotNull Class<? extends AbstractCommand> clazz : classes) {
            registry(clazz.newInstance());
        }
        @NotNull String command = "";
        while (!"exit".equals(command)) {
            command = input.readLine();
            try {
                execute(command);
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void execute(final @Nullable String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        final @Nullable AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        final @NotNull List<String> commandList = new ArrayList<>();
        commandList.add("about");
        commandList.add("help");
        commandList.add("user-create");
        commandList.add("user-login");
        if (commandList.contains(abstractCommand.command())) abstractCommand.execute();
        else if (sessionDTO != null) abstractCommand.execute();
    }

    public void registry(final @NotNull AbstractCommand command) throws Exception {
        final @NotNull String cliCommand = command.command();
        final @NotNull String cliDescription = command.description();
        if (cliCommand.isEmpty()) throw new Exception();
        if (cliDescription.isEmpty()) throw new Exception();
        command.setServiceLocator(this);
        command.setBootstrap(this);
        commands.put(cliCommand, command);
    }
}