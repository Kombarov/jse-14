package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.*;

public final class UserViewCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-view";
    }

    @NotNull
    @Override
    public String description() {
        return "Show user info.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[USER VIEW]");
        if (serviceLocator == null) throw new Exception();
        final @NotNull String userId = serviceLocator.getSessionDTO().getUserId();
        printUser(serviceLocator.getUserEndpoint().findOneUser(serviceLocator.getSessionDTO(), userId));
        printProjects(serviceLocator.getProjectEndpoint().findAllProjectsByUserId(serviceLocator.getSessionDTO()));
        System.out.println();
        System.out.println("tasks:");
        printTasks(serviceLocator.getTaskEndpoint().findAllTasksByUserId(serviceLocator.getSessionDTO()));
        System.out.println("[OK]");
    }
}
