package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printTask;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskSelectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-select";
    }

    @NotNull
    @Override
    public String description() {
        return "Select the task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SELECT]");
        if (serviceLocator == null) throw new Exception();
        printTasks(serviceLocator.getTaskEndpoint().findAllTasksByUserId(serviceLocator.getSessionDTO()));
        System.out.println("ENTER TASK NAME");
        final @Nullable String taskId = serviceLocator.getTaskEndpoint().findTasksByName(serviceLocator.getSessionDTO(), input.readLine()).getId();
        printTask(serviceLocator.getTaskEndpoint().findOneTask(serviceLocator.getSessionDTO(), taskId));
        System.out.println("[OK]");
    }
}
