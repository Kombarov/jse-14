package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printProjects;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskShowByProjectCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-show by project";
    }

    @NotNull
    @Override
    public String description() {
        return "Show tasks by project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SHOW BY PROJECT]");
        if (serviceLocator == null) throw new Exception();
        printProjects(serviceLocator.getProjectEndpoint().findAllProjectsByUserId(serviceLocator.getSessionDTO()));
        System.out.println("ENTER PROJECT NAME");
        final @Nullable String projectId = serviceLocator.getProjectEndpoint().findProjectByName(serviceLocator.getSessionDTO(), input.readLine()).getId();
        printTasks(serviceLocator.getTaskEndpoint().getTaskListByProjectId(serviceLocator.getSessionDTO(), projectId));
        System.out.println("[OK]");
    }
}
