package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-remove";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK REMOVE]");
        if (serviceLocator == null) throw new Exception();
        printTasks(serviceLocator.getTaskEndpoint().findAllTasksByUserId(serviceLocator.getSessionDTO()));
        System.out.println("ENTER TASK NAME FOR REMOVE");
        final @Nullable String taskId = serviceLocator.getTaskEndpoint().findTasksByName(serviceLocator.getSessionDTO(), input.readLine()).getId();
        serviceLocator.getTaskEndpoint().removeTask(serviceLocator.getSessionDTO(), taskId);
        System.out.println("[OK]");
    }
}
