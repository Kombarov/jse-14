package ru.kombarov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printProjects;

public class ProjectListByDateFinishCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "project-list show by finish date";
    }

    @NotNull
    @Override
    public String description() {
        return "Show projects by finish date.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST BY FINISH DATE]");
        if (serviceLocator == null) throw new Exception();
        printProjects(serviceLocator.getProjectEndpoint().sortProjectsByDateFinish(serviceLocator.getSessionDTO(), serviceLocator.getSessionDTO().getUserId()));
        System.out.println("[OK]");
    }
}
