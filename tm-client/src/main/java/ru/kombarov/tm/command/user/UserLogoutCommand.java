package ru.kombarov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.SessionDTO;
import ru.kombarov.tm.command.AbstractCommand;

public final class UserLogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "user-logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("USER LOG OUT");
        final @Nullable SessionDTO currentSession = serviceLocator.getSessionDTO();
        serviceLocator.getSessionEndpoint().removeSession(currentSession.getUserId(), currentSession.getId());
        serviceLocator.setSessionDTO(null);
        System.out.println("[OK]");
    }
}
