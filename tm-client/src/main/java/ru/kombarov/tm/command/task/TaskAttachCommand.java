package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.TaskDTO;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.EntityUtil.printProjects;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskAttachCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-attach";
    }

    @NotNull
    @Override
    public String description() {
        return "Attach task to project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK ATTACH]");
        if (serviceLocator == null) throw new Exception();
        printTasks(serviceLocator.getTaskEndpoint().findAllTasksByUserId(serviceLocator.getSessionDTO()));
        System.out.println("ENTER TASK NAME");
        final @NotNull String taskName = input.readLine();
        printProjects(serviceLocator.getProjectEndpoint().findAllProjectsByUserId(serviceLocator.getSessionDTO()));
        System.out.println("ENTER PROJECT NAME TO ATTACH TASK");
        final @Nullable String projectName = input.readLine();
        final @Nullable String taskId = serviceLocator.getTaskEndpoint().findTasksByName(serviceLocator.getSessionDTO(), taskName).getId();
        final @Nullable TaskDTO task = serviceLocator.getTaskEndpoint().findOneTask(serviceLocator.getSessionDTO(), taskId);
        if (task == null) throw new Exception();
        task.setProjectId(serviceLocator.getProjectEndpoint().findProjectByName(serviceLocator.getSessionDTO(), projectName).getId());
        System.out.println("[OK]");
    }
}
