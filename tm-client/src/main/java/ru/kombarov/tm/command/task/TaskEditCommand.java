package ru.kombarov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kombarov.tm.api.endpoint.TaskDTO;
import ru.kombarov.tm.command.AbstractCommand;

import static ru.kombarov.tm.util.DateUtil.parseStringToDate;
import static ru.kombarov.tm.util.EntityUtil.printTasks;

public final class TaskEditCommand extends AbstractCommand {

    @NotNull
    @Override
    public String command() {
        return "task-edit";
    }

    @NotNull
    @Override
    public String description() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK EDIT]");
        if (serviceLocator == null) throw new Exception();
        printTasks(serviceLocator.getTaskEndpoint().findAllTasksByUserId(serviceLocator.getSessionDTO()));
        System.out.println("ENTER TASK NAME FOR EDIT");
        final @Nullable String nameAnotherTask = input.readLine();
        final @NotNull TaskDTO anotherTask = new TaskDTO();
        anotherTask.setName(nameAnotherTask);
        System.out.println("ENTER TASK DESCRIPTION");
        anotherTask.setDescription(input.readLine());
        System.out.println("ENTER START DATE");
        anotherTask.setDateStart(parseStringToDate(input.readLine()));
        System.out.println("ENTER FINISH DATE");
        anotherTask.setDateFinish(parseStringToDate(input.readLine()));
        anotherTask.setId(serviceLocator.getTaskEndpoint().findTasksByName(serviceLocator.getSessionDTO(), nameAnotherTask).getId());
        anotherTask.setUserId(serviceLocator.getSessionDTO().getUserId());
        serviceLocator.getTaskEndpoint().mergeTask(serviceLocator.getSessionDTO(), anotherTask);
        System.out.println("[OK]");
    }
}
